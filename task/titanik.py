import pandas as pd

prefixes = ["Mr.", "Mrs.", "Miss."]

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df

def prefix(name):
    for pre in prefixes:
        if pre in name:
    return pre

def get_filled():
    lst = []
    titanicDf = get_titatic_dataframe()
    titanicDf["Prefix"] = titanicDf["Name"].apply(prefix)

    for pre in prefixes:
        preDf = titanicDf[titanicDf["Prefix"] == pre]
        count = preDf['Age'].isna().sum()
        median = preDf[preDf['Age'].notna()]['Age'].median()
        lst.append((pre, count, int(median)))

    return lst